<?php

/**
 * @file
 * Contains \Drupal\root_favicon\Controller\FaviconController.
 */

namespace Drupal\root_favicon\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Theme\ThemeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

/**
 * Return favicon.ico.
 */
class FaviconController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The theme manager.
   * 
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Constructs a FaviconController object.
   *
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   *   The theme manager service.
   */
  public function __construct(ThemeManagerInterface $theme_manager) {
    $this->themeManager = $theme_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('theme.manager')
    );
  }

  /**
   * Return active theme favicon.ico
   *
   * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
   */
  public function themeFavicon() {
    $favicon_path = DRUPAL_ROOT . DIRECTORY_SEPARATOR . $this->themeManager->getActiveTheme()->getPath() . DIRECTORY_SEPARATOR . 'favicon.ico';
    if (!is_readable($favicon_path)) {
      throw new NotFoundHttpException;
    }

    $response = new BinaryFileResponse($favicon_path);
    return $response;
  }

}
